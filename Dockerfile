FROM node:10-alpine
WORKDIR /app
ADD . /app
RUN npm install 
RUN npm run build
EXPOSE 3000
CMD ["npm", "run", "start:dev"]